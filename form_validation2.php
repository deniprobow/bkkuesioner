<?php
session_start();

$koneksi = new mysqli("localhost","root","","bk"); 

if(!isset($_SESSION['email']))
{
	echo "<script>alert('Anda Harus Login Terlebih Dahulu')</script>";
	echo "<script>location='login.php';</script>";
	header('location:login.php');
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>PT. Bujang Sejahtera Abadi</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <script src="lib/chart-master/Chart.js"></script>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index2.php" class="logo"><b>CM<span>O</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <li class="mt">
            <a class="active" href="index2.php">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Cmo</span>
              </a>
            <ul class="sub">
              <li><a href="cmo.php">Cmo</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Data Hasil</span>
              </a>
            <ul class="sub">
              <li><a href="hasil_cmo.php">Hasil</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Kuesioner</span>
              </a>
            <ul class="sub">
              <li><a href="c_presepsi.php">Kuesioner Presepsi</a></li>
              <li><a href="c_harapan.php">Kuesioner Harapan</a></li>
            </ul>
          </li>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Akun</span>
              </a>
            <ul class="sub">
              <li><a href="user.php">User</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Data Hasil</span>
              </a>
            <ul class="sub">
              <li><a href="hasil.php">Hasil</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Kuesioner</span>
              </a>
            <ul class="sub">
              <li><a href="k_presepsi.php">Kuesioner Presepsi</a></li>
              <li><a href="k_harapan.php">Kuesioner Harapan</a></li>
            </ul>
          </li>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
                <div class="col-lg-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tambah Akun
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
					<form name="form1"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
							<div class="form-group">
							    <label> Nama</label>
								<input type="text"name="name" class="form-control"type="text" required />
                            </div>
							<div class="form-group">
								<label>Email</label>
								<input type="text" name="email" id="email1"class="form-control" type="text" required / >
								<div id="ket">
							 </div>
							 <div class="form-group">
								<label>Password</label>
								<input type="password" name="password" class="form-control" type="text" required />
						   </div>
						   <br>
						   <div class="row">
						   <div class="col-lg-6">
						    <div class="form-group">
                        <select name="level" class="form-control" required>
		                <option value="">Pilih Level</option>
                        <option value="cmo">Cmo</option>
                        </select>
                           </div>
							<button class="btn btn-theme" name="save">save</button>
							<a href="cmo.php?halaman=cmo" class="btn btn-primary">batal</a>
					</form >
					<?php
					if (isset($_POST['save']))
					{
						{
		
						$koneksi->query("INSERT INTO cmo SET nama='$_POST[name]', email='$_POST[email]',password='$_POST[password]', level='$_POST[level]'");
						}
						echo "<script>alert('data cmo telah disimpan');</script>";
						echo "<script>location='cmo.php'</script>";
					}
					?>
						</div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

						</div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <!--custom switch-->
  <script src="lib/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="lib/jquery.tagsinput.js"></script>

  <!--Contactform Validation-->
  <script src="lib/php-mail-form/validate.js"></script>

<script type="text/javascript">
	   
	   $(document).ready(function(){
		$('#email1').on('change',function(){
				var email12= $("#email1").val();
			$.ajax({url:"validasi_email.php?email="+email12, success: function(result){
			$("#ket").html(result);
			}});
		});
	});
</script>
<script>
 function validateForm() {
 var g = $("#ket").html();
 if (g=="email sudah ada harap ganti email") {
 alert("email sudah digunakan");
 return false;
 }
 }

 </script>

