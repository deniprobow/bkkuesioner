<?php
session_start();

$koneksi = new mysqli("localhost","root","","bk"); 

if(!isset($_SESSION['email']))
{
	echo "<script>alert('Anda Harus Login Terlebih Dahulu')</script>";
	echo "<script>location='login.php';</script>";
	header('location:login.php');
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Bujang - Harapan</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <script src="lib/chart-master/Chart.js"></script>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index3.php" class="logo"><b>US<span>ER</span></b></a>
     <div class="top-menu">
        <ul class="nav pull-right top-menu">
           <li><a class="logout" href="logout.php"><i class="fa fa-sign-out fa-1x"></i></button></a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <?php include "menu_user.php"; ?>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

        <style>
          ul li.active a{
            background-color: #eaeaea !important;
          }

        </style>
        <ul class="nav nav-tabs visible-xs mt">
          <li class="<?php if ((stripos($_SERVER['REQUEST_URI'],'u_presepsi.php') !== false) ) {echo 'active';} ?>"><a href="u_presepsi.php">PRESEPSI</a></li>
          <li class="<?php if ((stripos($_SERVER['REQUEST_URI'],'u_harapan.php') !== false) ) {echo 'active';} ?>"><a href="u_harapan.php">HARAPAN</a></li>
          <li class="<?php if ((stripos($_SERVER['REQUEST_URI'],'u_feedback.php') !== false) ) {echo 'active';} ?>"><a href="u_feedback.php">FEEDBACK</a></li>
        </ul>

        <h3 class='text-center'> Tabel Kuesioner Harapan</h3>
        <?php 
        $disabled = "";
        $cek = $koneksi->query("SELECT * FROM input_harapan where id_user='$_SESSION[id_user]'")->num_rows;
        if($cek>0){
          $disabled = "disabled";
        ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>PERHATIAN!</strong> Anda telah mengisi data kuesioner harapan.
        </div>
        <?php } ?>
        <div class="row mb">
          <!-- page start-->
          <div class="panel-body">
							<form action='input_harapan.php' method='POST'>
							<?php $ambil2=$koneksi->query("SELECT * from dimensi");?>
							<?php while($dimensi = $ambil2->fetch_assoc()){ ?>
							<h5><b><?php echo $dimensi['nama_dimensi'] ?></b></h5>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kuesioner</th>
                                        <th style='width:40%'>Jawaban</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
								<?php $nomor=1;?>
                                <?php $ambil=$koneksi->query("SELECT  *
FROM kuesioner_harapan
LEFT JOIN input_harapan on input_harapan.id_harapan=kuesioner_harapan.id_harapan
where id_dimensi = $dimensi[id_dimensi]");?>
                                <?php while($kuesioner = $ambil->fetch_assoc()){ ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $nomor; ?></td>
                                        <td><?php echo $kuesioner['isi_kuesioner']; ?></td>
                                        <td>
											<select name='jawaban<?php echo $kuesioner['id_harapan'] ?>' class='form-control input-sm'>
												
                        <?php
                        $jawaban = array(1=>"Sangat Tdk Terpenuhi","Tdk Terpenuhi","Cukup Terpenuhi","Terpenuhi","Sangat Terpenuhi");
                        for ($rsv=1;$rsv<=count($jawaban);$rsv++) {
                          if($rsv==$kuesioner['nilai'])
                          echo "<option value='$rsv' selected>".$jawaban[$rsv]."</option>";
                          else
                          echo "<option value='$rsv'>".$jawaban[$rsv]."</option>";
                        }
                        ?>
											</select>
										</td>
                                      
                                    </tr>
									<?php $nomor++; ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } ?>
              <button class='btn btn-primary btn-sm mr' <?php echo $disabled ?>>INPUT</button>
							<button class='btn btn-default btn-sm' type='reset'>RESET</button>
							</form>
                        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
