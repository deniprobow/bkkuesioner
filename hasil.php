<?php
error_reporting(0);
session_start();

$koneksi = new mysqli("localhost","root","","bk"); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Admin - Hasil</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>ADM<span>IN</span></b></a>
      <div class="top-menu">
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Akun</span>
              </a>
            <ul class="sub">
              <li><a href="user.php">User</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Hasil</span>
              </a>
            <ul class="sub">
              <li><a href="responden.php">Responden</a></li>
              <li><a href="hasil.php">Nilai Rata-Rata</a></li>
			  <li><a href="dimensi.php">Rata-Rata 5 Dimensi</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Kuesioner</span>
              </a>
            <ul class="sub">
              <li><a href="k_presepsi.php">Kuesioner Presepsi</a></li>
              <li><a href="k_harapan.php">Kuesioner Harapan</a></li>
            </ul>
          </li>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
	  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Tabel Hasil Kuesioner</h3>
		<a href="cetak.php" class="btn btn-primary"><i class="fa fa-print fa-1x"></i> Cetak</a>
        <div class="row mb">
          <!-- page start-->
          <div class="panel-body">
                            <table width="100%" class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                     <tr>
                                        <th rowspan=2>Atribut Pernyataan</th>
                                        <th colspan=2>Harapan Pelayanan</th>
                                        <th colspan=2>Kenyataan Pelayanan</th>
										<th rowspan=2>Nilai Gap 5</th>
                                    </tr>
									<tr>
										
                                        <th>Nilai Pembobotan</th>
                                        <th>Rata-Rata Harapan</th>
										<th>Nilai Pembobotan</th>
										<th>Rata-Rata Kenyataan</th>
									</tr>
                                </thead>
                                 <tbody>
								<?php $nomor=1;?>
                                <?php $ambil=$koneksi->query("SELECT * FROM kuesioner_harapan ORDER BY id_harapan asc");?>
                                <?php for($p=1;$p<=$ambil->num_rows;$p++){
                                	$px = $p-1;
                                	$rsh=  $koneksi->query("select * from kuesioner_harapan order by id_harapan asc limit 1, $p")->fetch_array();
                                	$rsk=  $koneksi->query("select * from kuesioner_presepsi order by id_pernyataan asc limit 1, $p")->fetch_array();
									$rsnh1 = $koneksi->query("select * from input_harapan where nilai=1 and id_harapan=$rsh[id_harapan]")->num_rows;
									$rsnh2 = $koneksi->query("select * from input_harapan where nilai=2 and id_harapan=$rsh[id_harapan]")->num_rows;
									$rsnh3 = $koneksi->query("select * from input_harapan where nilai=3 and id_harapan=$rsh[id_harapan]")->num_rows;
									$rsnh4 = $koneksi->query("select * from input_harapan where nilai=4 and id_harapan=$rsh[id_harapan]")->num_rows;
									$rsnh5 = $koneksi->query("select * from input_harapan where nilai=5 and id_harapan=$rsh[id_harapan]")->num_rows;
									$rsnuh = $koneksi->query("select distinct(id_user) from input_harapan where id_harapan=$rsh[id_harapan]")->num_rows;
									$totrsnh1= ($rsnh1+$rsnh2+$rsnh3+$rsnh4+$rsnh5);
									$totrsnh2= ($rsnh1+$rsnh2+$rsnh3+$rsnh4+$rsnh5)/$rsnuh;
									
									$rsnk1 = $koneksi->query("select * from input_presepsi where nilai=1 and id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$rsnk2 = $koneksi->query("select * from input_presepsi where nilai=2 and id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$rsnk3 = $koneksi->query("select * from input_presepsi where nilai=3 and id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$rsnk4 = $koneksi->query("select * from input_presepsi where nilai=4 and id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$rsnk5 = $koneksi->query("select * from input_presepsi where nilai=5 and id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$rsnuk = $koneksi->query("select distinct(id_user) from input_presepsi where id_pernyataan=$rsk[id_pernyataan]")->num_rows;
									$totrsnk1= ($rsnk1+$rsnk2+$rsnk3+$rsnk4+$rsnk5);
									$totrsnk2= ($rsnk1+$rsnk2+$rsnk3+$rsnk4+$rsnk5)/$rsnuk;

									$nilaigap = $totrsnk2 - $totrsnh2;
								?>
                                    <tr class="gradeX">
                                        <td><?php echo $nomor; ?></td>
                                        <td><?php echo $totrsnh1; ?></td>
                                        <td><?php echo $totrsnh2; ?></td>
                                        <td><?php echo $totrsnk1; ?></td>
                                        <td><?php echo $totrsnk2; ?></td>
                                        <td><?php echo $nilaigap; ?></td>
                                    </tr>
									<?php $nomor++;
											$totalh +=$totrsnh2;	
											$totalk +=$totrsnk2;	
									 ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                	<tr>
                                		<td>Jumlah</td>
                                		<td> </td>
                                		<td><?php echo $totalh ?></td>
                                		<td> </td>
                                		<td><?php echo $totalk ?></td>
                                		<td> </td>
                                	</tr>
                                </tfoot>
                            </table>
							<br>
							
                        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
	<!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
