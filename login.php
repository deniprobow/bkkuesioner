<?php
session_start();
$koneksi = new mysqli("localhost","root","","bk"); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Bujang - Login</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  
  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
	  *********************************************************************************************************************************************************** -->
<br>
 <p class="centered"><img src="img/1.jpg" class="img-circle" width="100"></a></p>
 <div id="login-page">
    <div class="container">
      <form class="form-login"method="POST">
        <h2 class="form-login-heading">Silahkan Login</h2>
        <div class="login-wrap">
          <input type="text" class="form-control" name="email"placeholder="Email" autofocus>
          <br>
          <input type="password" class="form-control" name="password"placeholder="Password">
          </br>
		  <div class="form-group">
           <select name="level" class="form-control" required>
		   <option value="">Pilih Level</option>
           <option  value="admin">Admin</option>
           <option value="cmo">Cmo</option>
		   <option value="user">User</option>
           </select>
           </div>
          <br>
          <button class="btn btn-theme btn-block"type="submit" name="login"><i class="fa fa-key"></i> LOGIN</button>
          </br>
		  <html>
          <div class="registration">
             &copy; <strong>PT. Bujang Sejahtera Abadi</strong> 
          </div>
		 <?php
				include("koneksi.php");
				if(isset($_POST['login'])){
											
					$email	= $_POST['email'];
					$password	= $_POST['password'];
					$level		= $_POST['level'];
					
					$query = mysqli_query($koneksi, "SELECT * FROM user WHERE email='$email' AND password='$password' AND level='$level'");
					$rowcount=mysqli_num_rows($query);
					if( $rowcount=='0'){
					echo"Proses Login Gagal, silahkan Login Kembali";
					}
					else{
						$row = mysqli_fetch_assoc($query);
					
							if($level	== 'admin'){
								echo $_SESSION['email']=$email;
								echo $_SESSION['level']='admin';
								header("Location: index.php");
							}else if($level	 == 'cmo'){
								$_SESSION['email']=$email;
								$_SESSION['level']='cmo';
								header("Location: index2.php");
							}else if ($level  == 'user'){
								 echo $_SESSION['id_user']=$row['id_user'];
								 echo $_SESSION['email']=$email;
								 echo $_SESSION['level']='user';
								 header("location: u_presepsi.php");
							}else{
								echo '<div class="alert alert-danger">Upss...!!! Login gagal.</div>';
							}
					                                   }
				        }
				else{
					
				}
				?>
        </div>
        <!-- modal -->
      </form>
	 
    </div>
	
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("img/Cocktail_Stemware_Spray_502278.jpg", {
      speed: 500
    });
  </script>
</body>

</html>
