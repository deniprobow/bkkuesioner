<?php
session_start();

$koneksi = new mysqli("localhost","root","","bk"); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Admin - Presepsi</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>ADM<span>IN</span></b></a>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Akun</span>
              </a>
            <ul class="sub">
              <li><a href="user.php">User</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Data Hasil</span>
              </a>
            <ul class="sub">
             <li><a href="hasil.php">Rata-Rata Harapan</a></li>
			  <li><a href="hasil.php">Rata-Rata 5 Dimensi</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Kuesioner</span>
              </a>
            <ul class="sub">
              <li><a href="k_presepsi.php">Kuesioner Presepsi</a></li>
              <li><a href="k_harapan.php">Kuesioner Harapan</a></li>
            </ul>
          </li>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
	 <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Tambah Kuesioner Presepsi</h3>
        <div class="row mb">
		 <div class="panel-body">
		 
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Isi Kuesioner
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form name="form1"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
								<div class="form-group">
									<label>Pertanyaan</label>
									<input type="text" name="isi_kuesioner" id="kuesioner"class="form-control" type="text" required / >
								<div id="ket">
								</div>
								<br>
					<div class="row">
						<div class="col-lg-6">
								<div class="form-group">
                        <select name="id_dimensi" class="form-control" required>
		                <option value="">Pilih Dimensi</option>
                        <option  value="1">Tangibles</option>
                        <option value="2 ">Reliability </option>
						<option value="3 ">Responsiveness </option>
						<option value="4 ">Assurance </option>
						<option value="5 ">Emphaty </option>
                        </select>
                                </div>
								
								<button class="btn btn-primary" name="save"><i class="fa fa-save fa-1x"></i></button>
								<a href="k_presepsi.php" class="btn btn-primary"><i class="fa fa-times fa-1x"></i></a>
								</br>
							</form >
					<?php
						if (isset($_POST['save']))
						{
							{
							$koneksi->query("INSERT INTO kuesioner_presepsi(isi_kuesioner,id_dimensi) VALUES('$_POST[isi_kuesioner]','$_POST[id_dimensi]')");
							
                            }
						echo  "<div class='alert alert-info'>Data Tersimpan</div>";
						echo  "<meta http-equiv='refresh' content='1;url=k_presepsi.php'>";
						}
					?>
						</div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
         <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
 
 <script type="text/javascript">
	   
	   $(document).ready(function(){
		$('#kuesioner').on('change',function(){
				var isi_kuesioner= $("#kuesioner").val();
			$.ajax({url:"validasi_kuesioner1.php?isi_kuesioner="+isi_kuesioner, success: function(result){
			$("#ket").html(result);
			}});
		});
	});
</script>
<script>
 function validateForm() {
 var g = $("#ket").html();
 if (g=="pertanyaan serupa sudah ada, ganti dengan pertanyaan lain") {
 alert("pertanyaan serupa ada di dalam data base");
 return false;
 }
 }

 </script>