<?php
session_start();

$koneksi = new mysqli("localhost","root","","bk"); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Admin - Harapan</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>ADM<span>IN</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/1.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Bujang Kurir</h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-home"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Akun</span>
              </a>
            <ul class="sub">
              <li><a href="user.php">User</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Data Hasil</span>
              </a>
            <ul class="sub">
              <li><a href="hasil.php">Rata-Rata Harapan</a></li>
			  <li><a href="hasil.php">Rata-Rata 5 Dimensi</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Kuesioner</span>
              </a>
            <ul class="sub">
              <li><a href="k_presepsi.php">Kuesioner Presepsi</a></li>
              <li><a href="k_harapan.php">Kuesioner Harapan</a></li>
            </ul>
          </li>
          <li>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
	
	    <section id="main-content">
      <section class="wrapper">
	  <div class="row">
         <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kuesioner
                        </div>
                        <!-- /.panel-heading -->
                    <div class="panel-body">
						<?php
							$ambil=$koneksi->query("SELECT * FROM kuesioner_harapan WHERE id_harapan='$_GET[id]'");
							$pecah=$ambil->fetch_assoc();
						?>
				<form name="form1"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
						<div class="form-group">
							<label>Pertanyaan</label>
							<input type="text"name="isi_kuesioner" id="isi_kuesioner" class="form-control"value="<?php echo $pecah ['isi_kuesioner'];?>" type="text" required />
							<div id="ket">
						</div>
						<br>
						    <button class="btn btn-primary" name="ubah"><i class="fa fa-save fa-1x"></i></button>
							<a href="k_harapan.php" class="btn btn-primary"><i class="fa fa-times fa-1x"></i></a>
						</br>
				</form >
					<?php
					if (isset($_POST['ubah']))
					{
						{
		
						$koneksi->query("UPDATE kuesioner_harapan SET isi_kuesioner='$_POST[isi_kuesioner]' WHERE id_harapan='$_GET[id]'");
						
						}
						echo "<script>alert('data kuesioner telah di ubah');</script>";
						echo "<script>location='k_harapan.php';</script>";
					}
					?>
						</div>
                    </div>
                    <!-- /.panel -->

						</div>
    </div>
                    <!-- /.panel -->
    </div>
                <!-- /.col-lg-12 -->
            </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

 <script type="text/javascript">
	   
	   $(document).ready(function(){
		$('#isi_kuesioner').on('change',function(){
				var isi_kuesioner= $("#isi_kuesioner").val();
			$.ajax({url:"validasi_harapan.php?isi_kuesioner="+isi_kuesioner, success: function(result){
			$("#ket").html(result);
			}});
		});
	});
</script>
<script>
 function validateForm() {
 var g = $("#ket").html();
 if (g=="pertanyaan serupa sudah ada, ganti dengan pertanyaan lain") {
 alert("pertanyaan sudah ada di dalam data base");
 return false;
 }
 }

 </script>
					