-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08 Apr 2019 pada 03.29
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.1.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dimensi`
--

CREATE TABLE `dimensi` (
  `id_dimensi` int(30) NOT NULL,
  `nama_dimensi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dimensi`
--

INSERT INTO `dimensi` (`id_dimensi`, `nama_dimensi`) VALUES
(1, 'Tangibles'),
(2, 'Reliability'),
(3, 'Responsiveness'),
(4, 'Assurance'),
(5, 'Emphaty');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil`
--

CREATE TABLE `hasil` (
  `id_pernyataan` int(11) NOT NULL,
  `id_dimensi` int(200) NOT NULL,
  `nilai_harapan` varchar(20) NOT NULL,
  `rata_harapan` varchar(20) NOT NULL,
  `nilai_kenyataan` varchar(20) NOT NULL,
  `rata_kenyataan` varchar(20) NOT NULL,
  `nilai_gap5` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil`
--

INSERT INTO `hasil` (`id_pernyataan`, `id_dimensi`, `nilai_harapan`, `rata_harapan`, `nilai_kenyataan`, `rata_kenyataan`, `nilai_gap5`) VALUES
(1, 0, '33', '423', '333', '555', '45443'),
(2, 0, '33', '423', '333', '555', '45443');

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_feedback`
--

CREATE TABLE `input_feedback` (
  `id_feedback` int(10) NOT NULL,
  `id_user` int(11) NOT NULL,
  `isi_feedback` text NOT NULL,
  `tgl_input_feedback` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `input_feedback`
--

INSERT INTO `input_feedback` (`id_feedback`, `id_user`, `isi_feedback`, `tgl_input_feedback`) VALUES
(1, 33, 'rwartaedadasd', '2019-04-08 01:29:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_harapan`
--

CREATE TABLE `input_harapan` (
  `id_input_harapan` int(200) NOT NULL,
  `id_user` int(200) NOT NULL,
  `id_harapan` int(100) NOT NULL,
  `nilai` int(200) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `input_harapan`
--

INSERT INTO `input_harapan` (`id_input_harapan`, `id_user`, `id_harapan`, `nilai`, `tanggal`) VALUES
(1, 32, 72, 1, '2019-03-30 19:36:59'),
(2, 32, 73, 1, '2019-03-30 19:36:59'),
(3, 32, 74, 1, '2019-03-30 19:36:59'),
(4, 32, 75, 1, '2019-03-30 19:36:59'),
(5, 32, 76, 1, '2019-03-30 19:36:59'),
(6, 32, 77, 1, '2019-03-30 19:36:59'),
(7, 32, 78, 1, '2019-03-30 19:36:59'),
(8, 32, 79, 1, '2019-03-30 19:36:59'),
(9, 32, 80, 1, '2019-03-30 19:36:59'),
(10, 32, 81, 1, '2019-03-30 19:36:59'),
(11, 32, 82, 1, '2019-03-30 19:36:59'),
(12, 32, 83, 1, '2019-03-30 19:36:59'),
(13, 32, 84, 1, '2019-03-30 19:36:59'),
(14, 32, 85, 1, '2019-03-30 19:36:59'),
(15, 32, 86, 1, '2019-03-30 19:36:59'),
(16, 32, 87, 1, '2019-03-30 19:36:59'),
(17, 32, 88, 1, '2019-03-30 19:36:59'),
(18, 32, 89, 1, '2019-03-30 19:36:59'),
(19, 32, 90, 1, '2019-03-30 19:36:59'),
(20, 32, 91, 1, '2019-03-30 19:36:59'),
(21, 33, 72, 4, '2019-04-08 02:48:41'),
(22, 33, 73, 2, '2019-04-08 02:48:41'),
(23, 33, 74, 3, '2019-04-08 02:48:41'),
(24, 33, 75, 5, '2019-04-08 02:48:41'),
(25, 33, 76, 1, '2019-04-08 02:48:41'),
(26, 33, 77, 2, '2019-04-08 02:48:41'),
(27, 33, 78, 3, '2019-04-08 02:48:41'),
(28, 33, 79, 4, '2019-04-08 02:48:41'),
(29, 33, 80, 5, '2019-04-08 02:48:41'),
(30, 33, 81, 4, '2019-04-08 02:48:41'),
(31, 33, 82, 3, '2019-04-08 02:48:41'),
(32, 33, 83, 2, '2019-04-08 02:48:41'),
(33, 33, 84, 1, '2019-04-08 02:48:41'),
(34, 33, 85, 2, '2019-04-08 02:48:41'),
(35, 33, 86, 3, '2019-04-08 02:48:41'),
(36, 33, 87, 4, '2019-04-08 02:48:41'),
(37, 33, 88, 5, '2019-04-08 02:48:41'),
(38, 33, 89, 4, '2019-04-08 02:48:41'),
(39, 33, 90, 3, '2019-04-08 02:48:41'),
(40, 33, 91, 2, '2019-04-08 02:48:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_presepsi`
--

CREATE TABLE `input_presepsi` (
  `id_input_presepsi` int(200) NOT NULL,
  `id_user` int(200) NOT NULL,
  `id_pernyataan` int(100) NOT NULL,
  `nilai` int(200) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `input_presepsi`
--

INSERT INTO `input_presepsi` (`id_input_presepsi`, `id_user`, `id_pernyataan`, `nilai`, `tanggal`) VALUES
(1, 32, 124, 3, '2019-03-30 19:36:01'),
(2, 32, 125, 3, '2019-03-30 19:36:01'),
(3, 32, 126, 3, '2019-03-30 19:36:01'),
(4, 32, 127, 2, '2019-03-30 19:36:01'),
(5, 32, 128, 4, '2019-03-30 19:36:01'),
(6, 32, 129, 4, '2019-03-30 19:36:01'),
(7, 32, 130, 5, '2019-03-30 19:36:01'),
(8, 32, 131, 3, '2019-03-30 19:36:01'),
(9, 32, 132, 3, '2019-03-30 19:36:01'),
(10, 32, 133, 3, '2019-03-30 19:36:01'),
(11, 32, 134, 3, '2019-03-30 19:36:01'),
(12, 32, 135, 3, '2019-03-30 19:36:01'),
(13, 32, 136, 3, '2019-03-30 19:36:01'),
(14, 32, 137, 5, '2019-03-30 19:36:01'),
(15, 32, 138, 4, '2019-03-30 19:36:01'),
(16, 32, 139, 4, '2019-03-30 19:36:01'),
(17, 32, 140, 3, '2019-03-30 19:36:01'),
(18, 32, 141, 2, '2019-03-30 19:36:01'),
(19, 32, 142, 3, '2019-03-30 19:36:01'),
(20, 32, 143, 3, '2019-03-30 19:36:01'),
(21, 33, 124, 1, '2019-04-08 02:47:37'),
(22, 33, 125, 2, '2019-04-08 02:47:37'),
(23, 33, 126, 3, '2019-04-08 02:47:37'),
(24, 33, 127, 4, '2019-04-08 02:47:37'),
(25, 33, 128, 5, '2019-04-08 02:47:37'),
(26, 33, 129, 2, '2019-04-08 02:47:37'),
(27, 33, 130, 3, '2019-04-08 02:47:37'),
(28, 33, 131, 4, '2019-04-08 02:47:37'),
(29, 33, 132, 5, '2019-04-08 02:47:37'),
(30, 33, 133, 1, '2019-04-08 02:47:37'),
(31, 33, 134, 2, '2019-04-08 02:47:37'),
(32, 33, 135, 3, '2019-04-08 02:47:37'),
(33, 33, 136, 4, '2019-04-08 02:47:37'),
(34, 33, 137, 5, '2019-04-08 02:47:37'),
(35, 33, 138, 4, '2019-04-08 02:47:37'),
(36, 33, 139, 3, '2019-04-08 02:47:37'),
(37, 33, 140, 2, '2019-04-08 02:47:37'),
(38, 33, 141, 1, '2019-04-08 02:47:37'),
(39, 33, 142, 3, '2019-04-08 02:47:37'),
(40, 33, 143, 5, '2019-04-08 02:47:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuesioner_harapan`
--

CREATE TABLE `kuesioner_harapan` (
  `id_harapan` int(30) NOT NULL,
  `isi_kuesioner` varchar(200) NOT NULL,
  `id_dimensi` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuesioner_harapan`
--

INSERT INTO `kuesioner_harapan` (`id_harapan`, `isi_kuesioner`, `id_dimensi`) VALUES
(72, 'Apakah perusahaan sudah memiliki peralatan dan teknologi terbaru?', 1),
(73, 'Apakah karyawan perusahaan sudah berpakaian dan berpenampilan rapi?', 1),
(74, 'Apakah fasilitas fisik perusahaan sudah tampak menarik?', 1),
(75, 'Apakah kelengkapan dan atribut Karyawan perusahaan tampak menarik?', 1),
(76, 'Apakah ketepatan waktu dalam melayani pesanan pelanggan sudah tercapai?', 2),
(77, 'Apakah karyawan dalam memberikan pelayanan masih mengalami kesalahan?', 2),
(78, 'Apakah karyawan kami sudah memberikan informasi secara jelas sebelum pelayanan diberikan?', 2),
(79, 'Apakah anda selaku Pelanggan tidak harus menunggu terlalu lama dalam menunggu pesanan datang?', 2),
(80, 'Apakah karyawan kami sangat tanggap dalam menanggapi pesanan?', 3),
(81, 'Apakah karyawan sudah menerima dan melayani pelanggan dengan baik?', 3),
(82, 'Apakah karyawan perusahaan memberikan pelayan secara cepat dan tepat?', 3),
(83, 'Apakah pelanggan merasa dilayani dengan baik dan sesuai keinginan pelanggan?', 3),
(84, 'Apakah karyawan perusahaan mempunyai kemampuan dan pengetahuan dalam menjawab pertanyaan pelanggan?', 4),
(85, 'Apakah karyawan sopan kepada setiap pelanggan?', 4),
(86, 'Apakah karyawan melayani pelanggan dengan sikap meyakinkan sehingga pelanggan merasa aman dalam melakukan transaksi?', 4),
(87, 'Apakah karyawan menggunakan perlengkapan yang lengkap sehingga pesanan sampai sesuai harapan pelanggan?', 4),
(88, 'Apakah karyawan memberikan waktu pelayanan yang cukup kepada pelanggan?', 5),
(89, 'Apakah karyawan memberikan pelayanan yang sesuai keinginan dan kebutuhan pelanggan?', 5),
(90, 'Apakah karyawan bersikap sopan dan ramah kepada pelanggan?', 5),
(91, 'Apakah karyawan dan pihak perusahaan mendengarkan keluhan pelanggan dengan baik?', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuesioner_presepsi`
--

CREATE TABLE `kuesioner_presepsi` (
  `id_pernyataan` int(30) NOT NULL,
  `isi_kuesioner` varchar(200) NOT NULL,
  `id_dimensi` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuesioner_presepsi`
--

INSERT INTO `kuesioner_presepsi` (`id_pernyataan`, `isi_kuesioner`, `id_dimensi`) VALUES
(124, 'Perusahaan memiliki peralatan dan teknologi terbaru?', 1),
(125, 'Karyawan perusahaan berpakaian dan berpenampilan rapi', 1),
(126, 'Fasilitas fisik perusahaan tampak menarik', 1),
(127, 'Kelengkapan dan atribut Karyawan perusahaan tampak menarik', 1),
(128, 'Ketepatan waktu dalam melayani pesanan pelanggan', 2),
(129, 'Kesalahan minimum dalam memberikan layanan', 2),
(130, 'Karyawan memberikan informasi secara jelas sebelum pelayanan diberikan', 2),
(131, 'Pelanggan tidak menunggu terlalu lama dalam menunggu pesanan datang', 2),
(132, 'Karyawan tanggap dalam menanggapi pesanan', 3),
(133, 'Karyawan menerima dan melayani pelanggan dengan baik', 3),
(134, 'Karyawan perusahaan memberikan pelayan secara cepat dan tepat', 3),
(135, 'Pelanggan merasa dilayani dengan baik dan sesuai keinginan pelanggan', 3),
(136, 'Karyawan perusahaan mempunyai kemampuan dan pengetahuan dalam menjawab pertanyaan pelanggan', 4),
(137, 'Karyawan sopan kepada setiap pelanggan', 4),
(138, 'Karyawan melayani pelanggan dengan sikap meyakinkan sehingga pelanggan merasa aman dalam melakukan transaksi', 4),
(139, 'Karyawan menggunakan perlengkapan yang lengkap sehingga pesanan sampai sesuai harapan pelanggan', 4),
(140, 'Karyawan memberikan waktu pelayanan yang cukup kepada pelanggan', 5),
(141, 'Karyawan memberikan pelayanan yang sesuai keinginan dan kebutuhan pelanggan', 5),
(142, 'Karyawan bersikap sopan dan ramah kepada pelanggan', 5),
(143, 'Karyawan dan pihak perusahaan mendengarkan keluhan pelanggan dengan baik', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama`, `email`, `level`) VALUES
(13, 'rani efendi', 'admin', 'Admin', 'ranytomkett@gmail.com', 'admin'),
(31, '', 'cmo', 'Cmo', 'ranytomkett@yahoo.co.id', 'cmo'),
(32, 'rani efendi', 'user', 'User', 'ranyefendy94@gmail.com', 'user'),
(33, '', 'test', '', 'test@gmail.com', 'user'),
(34, '', 'test2', 'test', 'test2@gmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dimensi`
--
ALTER TABLE `dimensi`
  ADD PRIMARY KEY (`id_dimensi`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id_pernyataan`);

--
-- Indexes for table `input_feedback`
--
ALTER TABLE `input_feedback`
  ADD PRIMARY KEY (`id_feedback`);

--
-- Indexes for table `input_harapan`
--
ALTER TABLE `input_harapan`
  ADD PRIMARY KEY (`id_input_harapan`);

--
-- Indexes for table `input_presepsi`
--
ALTER TABLE `input_presepsi`
  ADD PRIMARY KEY (`id_input_presepsi`);

--
-- Indexes for table `kuesioner_harapan`
--
ALTER TABLE `kuesioner_harapan`
  ADD PRIMARY KEY (`id_harapan`);

--
-- Indexes for table `kuesioner_presepsi`
--
ALTER TABLE `kuesioner_presepsi`
  ADD PRIMARY KEY (`id_pernyataan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dimensi`
--
ALTER TABLE `dimensi`
  MODIFY `id_dimensi` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id_pernyataan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `input_feedback`
--
ALTER TABLE `input_feedback`
  MODIFY `id_feedback` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `input_harapan`
--
ALTER TABLE `input_harapan`
  MODIFY `id_input_harapan` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `input_presepsi`
--
ALTER TABLE `input_presepsi`
  MODIFY `id_input_presepsi` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `kuesioner_harapan`
--
ALTER TABLE `kuesioner_harapan`
  MODIFY `id_harapan` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `kuesioner_presepsi`
--
ALTER TABLE `kuesioner_presepsi`
  MODIFY `id_pernyataan` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
